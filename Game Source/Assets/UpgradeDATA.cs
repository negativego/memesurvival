using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UpgradeType
{
    WeapomUpgrade,
    ItemUpgrade,
    WeaponUnlock,
    ItemUnlock
}

[CreateAssetMenu]
public class UpgradeDATA : ScriptableObject
{
    public UpgradeType upgradetype;
    public string Name;
    public Sprite icon;
    public Sprite Discliption;

    public WeaponData weaponData;
    public WeaponStats weaponupgradeStats;

    public Item item;
    public ItemStats itemStats;
}
