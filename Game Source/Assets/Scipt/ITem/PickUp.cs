using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    

    public void OnTriggerEnter2D(Collider2D collision)
    {
        Character_Scipt c = collision.GetComponent<Character_Scipt>();
        if(c != null)
        {
            GetComponent<PickUpObject>().PickUp(c);
            Destroy(gameObject);
        }
    }
}
