using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropOndestroy : MonoBehaviour
{
    [SerializeField] GameObject DropIteamPrefab;

    private void OnDestroy()

    {
        Transform t = Instantiate(DropIteamPrefab).transform;
        t.position = transform.position;
    }

}
