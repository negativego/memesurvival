using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_Scipt : MonoBehaviour
{
    int level = 1;
    int exp = 0;
    [SerializeField] Expbar_Script expbar;
    [SerializeField] UpgradePanelManager upgradePanel;

    WeaponManager weaponManager;
    PassiveItem passiveItem;

    [SerializeField] List<UpgradeDATA> upgrades;
    List<UpgradeDATA> selecetUpgrade;

     [SerializeField] List<UpgradeDATA> acquiredUpgrades;
     [SerializeField] List<UpgradeDATA> upgradesAvailableOnStart;

    private void Awake()
    {
        weaponManager = GetComponent<WeaponManager>();
        passiveItem = GetComponent<PassiveItem>();
    }

    private void Start()
    {
        expbar.UpdateExpslider(exp, Tolevelup);
        expbar.SetlevelText(level);
        AddUpgradeIntotheListOfAvailableUpgrades(upgradesAvailableOnStart);
    }

    internal void AddUpgradeIntotheListOfAvailableUpgrades(List<UpgradeDATA> upgradesToAdd)
    {
        if(upgradesToAdd == null) { return; }
        this.upgrades.AddRange(upgradesToAdd);
    }

    int Tolevelup
    {
        get
        {
            return level * 1000;
        }
    }

    public void Addexp(int amount)
    {
        exp += amount;
        CheckLevelup();
        expbar.UpdateExpslider(exp, Tolevelup);
    }

    public void CheckLevelup()
    {
        if (exp >= Tolevelup)
        {
            LevelUp();
        }
    }

    public void Upgrade(int SelectedUpgradeID)
    {
        UpgradeDATA upgradeData = selecetUpgrade[SelectedUpgradeID];

        if(acquiredUpgrades == null) { acquiredUpgrades = new List<UpgradeDATA>(); }

        switch(upgradeData.upgradetype)
        {
            case UpgradeType.WeapomUpgrade:
                weaponManager.UpgradeWeapon(upgradeData);
                break;
            case UpgradeType.ItemUpgrade:
            passiveItem.UpgradeItem(upgradeData);
                break;
            case UpgradeType.WeaponUnlock:
                weaponManager.Addweapon(upgradeData.weaponData);
                break;
            case UpgradeType.ItemUnlock:
                passiveItem.Equip(upgradeData.item);
                AddUpgradeIntotheListOfAvailableUpgrades(upgradeData.item.upgrades);
                break;

        }

        acquiredUpgrades.Add(upgradeData);
        upgrades.Remove(upgradeData);
    }

    private void LevelUp()
    {
        if(selecetUpgrade == null) { selecetUpgrade = new List<UpgradeDATA>(); }
        selecetUpgrade.Clear();
        selecetUpgrade.AddRange(GetUpgrades(3));

        upgradePanel.OpenPanel(selecetUpgrade);
            exp -= Tolevelup; 
            level += 1;
            expbar.SetlevelText(level);
    }

    public void SuffleUpgrades()
    {
        for(int i = upgrades.Count - 1; i > 0; i--)
        {
            int x = Random.Range(0, i+1);
            UpgradeDATA shuffleElement = upgrades[i];
            upgrades[i] = upgrades[x];
            upgrades[x] = shuffleElement;
        }
    }

    public List<UpgradeDATA> GetUpgrades(int count)
    {
        List<UpgradeDATA> upgradeList = new List<UpgradeDATA>();


        if(count > upgrades.Count)
        {
            count = upgrades.Count;
        }

        for(int i = 0;i<count;i++)
        {
            upgradeList.Add(upgrades[i]);
        }
        return upgradeList;
    }
}

