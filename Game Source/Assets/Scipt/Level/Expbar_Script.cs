using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Expbar_Script : MonoBehaviour
{
    [SerializeField] Slider slider;
    [SerializeField] TMPro.TextMeshProUGUI leveltext;

    public void UpdateExpslider(int current, int target)
    {
        slider.maxValue = target;
        slider.value = current;
    }

    public void SetlevelText(int level)
    {
        leveltext.text = "LEVEL:" + level.ToString();
    }

}
