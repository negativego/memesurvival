using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Healt_bar_scipt : MonoBehaviour

{
    public Character_Scipt character;
    Slider Hpslider;

    public void Start()
    {
        Hpslider = GetComponent<Slider>();
    }

    public void SetmaxHp()
    {
        Hpslider.maxValue = 
            100;
        Hpslider.value = character.currentHp;
    }

    public void Sethp(float Hp)
    {
        Hpslider.value = Hp/character.maxHp*100;
        
    }
}
