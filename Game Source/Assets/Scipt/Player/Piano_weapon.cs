using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piano_weapon : WeaponDataBase
{
    [SerializeField] GameObject leftBonkObject;
    [SerializeField] GameObject RightBonkObject;
    [SerializeField] float attackAreaSize = 3f;
    private Vector2 BonkSize = new Vector2(4,2f);
    Playermove playermove;

    private void Awake()
    {
        playermove = GetComponentInParent<Playermove>() ; 
    }

    public override void Attack()
    {
        RightBonkObject.SetActive(true);
        leftBonkObject.SetActive(true);
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, attackAreaSize);
        ApplyDamage(colliders);
                     colliders = Physics2D.OverlapBoxAll(RightBonkObject.transform.position, BonkSize, 0f);
                     colliders = Physics2D.OverlapBoxAll(leftBonkObject.transform.position, BonkSize, 0f);
        
    }
}


    
    /*Playermove playermove;
    private Vector2 BonkSize = new Vector2(4,2f);

    private void Awake()
    {
        playermove = GetComponentInParent<Playermove>() ; 
    }
   

    private void ApplyDamage(Collider2D[] colliders)
    {
        for(int i =0; i < colliders.Length; i++ )
        {
            Debug.Log(colliders[i].gameObject.name);
            IDamagetable e = colliders[i].GetComponent<IDamagetable>();
            if(e != null)
            {

                PostDamage(weaponStats.damage, colliders[i].transform.position);
                e.Takedamage(weaponStats.damage);

            }

        }
    }*/
