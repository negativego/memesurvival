using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character_Scipt : MonoBehaviour
{
     public  float maxHp;
     public float currentHp;

    public int armor = 0;
   
    [SerializeField] Healt_bar_scipt Hpbar;

    [HideInInspector] public Level_Scipt level;

    private bool isdead;

    private void Awake()
    {
        level = GetComponent<Level_Scipt>();
    }

    public void Start()
    {
        Hpbar.SetmaxHp();
        
    }

    public void TakeDamage(float damage)
    {
        if (isdead == true){return;}
        ApplyArmor(ref damage);

        currentHp -= damage;

        if (currentHp <= 0)
        {
            GetComponent<CharacterGameOver>().GameOver();
            isdead = true;
        }
        Hpbar.Sethp(currentHp);
    }

    private void ApplyArmor(ref float damage)
    {
        damage -= armor;
        if (damage < 0)
        {
            damage = 0;
        }
    }

    public void Heal(float amount)
    {
        currentHp += amount; 
        if (currentHp > maxHp)
        {
            currentHp = maxHp;
        }
        Hpbar.Sethp(currentHp);
    }
}

