using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableBonk : MonoBehaviour
{
    float timeTodisable = 0.8f;
    float timer;

    private void OnEnable()
    {
        timer = timeTodisable;
    }

    private void LateUpdate()
    {
        timer -= Time.deltaTime;
        if(timer < 0f)
        {
            gameObject.SetActive(false);
        }
    }
}
