using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonk_Weapon : WeaponDataBase
{
  
    [SerializeField] GameObject leftBonkObject;
    [SerializeField] GameObject RightBonkObject;

    Playermove playermove;
    private Vector2 BonkSize = new Vector2(4,2f);

    private void Awake()
    {
        playermove = GetComponentInParent<Playermove>() ; 
    }
    public override void Attack()
    {
        Debug.Log("Attack to Meme");


        if (playermove.lastHorizontalCoupledVector >= -1)
        {
            RightBonkObject.SetActive(true);
            Collider2D[] colliders = Physics2D.OverlapBoxAll(RightBonkObject.transform.position, BonkSize, 0f);
            ApplyDamage(colliders);

        }
        else
        {


            leftBonkObject.SetActive(true);
            Collider2D[] colliders = Physics2D.OverlapBoxAll(leftBonkObject.transform.position, BonkSize, 0f);
            ApplyDamage(colliders);
        }

    }
}
 