using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))] 
public class Playermove : MonoBehaviour
{

    Rigidbody2D rgbd2d;
    public Vector3 movementVector;
    bool FecingRight = true ;
    public float lastHorizontalDeCoupledVector;
    public float lastVerticalDeCoupledVector;
    public float lastHorizontalCoupledVector;
    public float lastVerticalCoupledVector;
    
     [SerializeField] float speed = 3f;

    private void Awake()
    {
        rgbd2d = GetComponent<Rigidbody2D>();
        movementVector = new Vector3();
    }
   private void Start()
    {
        lastHorizontalDeCoupledVector = -1f;
        lastVerticalDeCoupledVector = 1f;

        lastHorizontalCoupledVector = -1f;
        lastVerticalCoupledVector = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        movementVector.x = Input.GetAxisRaw("Horizontal");
        movementVector.y = Input.GetAxisRaw("Vertical");

        if(movementVector.x != 0 || movementVector.y != 0)
        {
            lastHorizontalCoupledVector = movementVector.x;
            lastVerticalCoupledVector = movementVector.y;
        }

        if(movementVector.x != 0)
        {
            lastHorizontalDeCoupledVector = movementVector.x;
        }
        if (movementVector.y != 0)
        {
            lastVerticalDeCoupledVector = movementVector.y;
        }

        Flip();
 


            movementVector *= speed;

        rgbd2d.velocity = movementVector;

    }

   
    public void Flip()//Flip sprite 
    {
        if (FecingRight && movementVector.x < 0f || !FecingRight && movementVector.x > 0f)
        {
            FecingRight = !FecingRight;
            Vector3 localScale = transform.localScale;
            localScale.x *= -1;
            transform.localScale = localScale;
        }
    }
}
