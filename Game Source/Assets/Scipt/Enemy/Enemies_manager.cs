using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemiesSpawnGroup
{
    public EnemyData enemyData;
    public int count;
    public bool isBoss;

    public float repeatTimer;
    public float timeBetweenSpawns;
    public int repeatCount;

    public EnemiesSpawnGroup(EnemyData enemyData, int count, bool isBoss)
    {
        this.enemyData = enemyData;
        this.count = count;
        this.isBoss = isBoss;
    }

    public void SetRepeatSpawn(float timeBetweenSpawns, int repeatCount)
    {
        this.timeBetweenSpawns = timeBetweenSpawns;
        this.repeatCount = repeatCount;
        repeatTimer = timeBetweenSpawns;
    }
}

public class Enemies_manager : MonoBehaviour
{
    [SerializeField] StageProgress stageProgress;
    [SerializeField] GameObject enemy;
    [SerializeField] Vector2 spawnArea;
    //[SerializeField] float spawntimer;
    [SerializeField] GameObject player;
    float timer;

    List<Enemy_scipt> bossEnemiseList;
    int totalBossHealth;            
    int currentBossHealth;
  
    List<EnemiesSpawnGroup> enemiesSpawnGroupList;
    List<EnemiesSpawnGroup> repeatedSpawnGroupList;

    int spawnPerFrame = 2;

    private void Start()
    {
        player = GameManager.instance.playerTransform.gameObject;
        stageProgress = FindObjectOfType<StageProgress>();
    }

    public void AddGroupToSpawn(EnemyData enemyTospawn, int count, bool isBoss)
    {
        EnemiesSpawnGroup newGroupToSpawn = new EnemiesSpawnGroup(enemyTospawn, count, isBoss);

        if(enemiesSpawnGroupList == null) {enemiesSpawnGroupList = new List<EnemiesSpawnGroup>();}

        enemiesSpawnGroupList.Add(newGroupToSpawn);
    }

    public void spawnEnemy(EnemyData enemyTospawn ,bool isBoss)
    {
        Vector3 position = UtilityTools.GenerateRandomposition(spawnArea);

        position += player.transform.position;

        GameObject newEnemy = Instantiate(enemy);
        newEnemy.transform.position = position;
        newEnemy.GetComponent<Enemy_scipt>().SetTarger(player);

        Enemy_scipt newEnemyComponent = newEnemy.GetComponent<Enemy_scipt>();
        newEnemyComponent.SetTarger(player);
        newEnemyComponent.SetStats(enemyTospawn.stats);
        newEnemyComponent.UpdateStatsForProgress(stageProgress.Progress);

        if (isBoss == true) 
        {
            SpawnBossEnemy(newEnemyComponent);
        }

        newEnemy.transform.parent = transform;

        newEnemyComponent.InitSprite(enemyTospawn.animatedPrefab);
    }



    public void Update() 
    {
        ProcessSpawn();
        ProcessRepeatedSpawnGroups();
        UpdateBossHealth();
    }

    private void ProcessRepeatedSpawnGroups()
    {
        for(int i = repeatedSpawnGroupList.Count - 1; i >= 0; i--)
        {
            if(repeatedSpawnGroupList == null) { return; }
            repeatedSpawnGroupList[i].repeatTimer -= Time.deltaTime;
            if(repeatedSpawnGroupList[i].repeatTimer < 0)
            {
                repeatedSpawnGroupList[i].repeatTimer = repeatedSpawnGroupList[i].timeBetweenSpawns;
                AddGroupToSpawn(repeatedSpawnGroupList[i].enemyData, repeatedSpawnGroupList[i].count, repeatedSpawnGroupList[i].isBoss);
                repeatedSpawnGroupList[i].repeatCount -= 1;
                if(repeatedSpawnGroupList[i].repeatCount <= 0)
                {
                    repeatedSpawnGroupList.RemoveAt(i);
                }
            }
        }
    }

    private void ProcessSpawn()
    {
        if(enemiesSpawnGroupList == null){return;}

        for(int i = 0; i < spawnPerFrame; i++)
        {
            if(enemiesSpawnGroupList.Count > 0)
            {
                if(enemiesSpawnGroupList[0].count <= 0) {return;}
                spawnEnemy(enemiesSpawnGroupList[0].enemyData, enemiesSpawnGroupList[0].isBoss);
                enemiesSpawnGroupList[0].count -= 1;

                if(enemiesSpawnGroupList[0].count <= 0)
                {
                    enemiesSpawnGroupList.RemoveAt(0);
                }
            }
        }
    }

    private void UpdateBossHealth() 
    {
        if(bossEnemiseList == null){return;}
        if(bossEnemiseList.Count == 0){return;}
        currentBossHealth = 0;

        for(int i = 0; i < bossEnemiseList.Count; i++)
        {
            if(bossEnemiseList[i] == null){continue;}
            currentBossHealth += bossEnemiseList[i].stats.hp;
        }

       
    }



    private void SpawnBossEnemy(Enemy_scipt newBoss) 
    {
        if(bossEnemiseList == null) { bossEnemiseList = new List<Enemy_scipt>();}

        bossEnemiseList.Add(newBoss);

        totalBossHealth += newBoss.stats.hp;

        
    }

    public void AddRepeatedSpawn(StageEvent stageEvent, bool isBoss)
    {
        EnemiesSpawnGroup repeatSpawnGroup = new EnemiesSpawnGroup(stageEvent.enemyTospawn, stageEvent.count, isBoss);
        repeatSpawnGroup.SetRepeatSpawn(stageEvent.repeatEverySeconds, stageEvent.repeatCount);

        if(repeatedSpawnGroupList == null)
        {
            repeatedSpawnGroupList = new List<EnemiesSpawnGroup>();
        }

        repeatedSpawnGroupList.Add(repeatSpawnGroup);
    }
}

