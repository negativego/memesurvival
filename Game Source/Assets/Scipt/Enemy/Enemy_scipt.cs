using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class EnemyStats
{
    public int hp = 20;
    public int damage = 1;
    public int exp_reward = 400;
    public float speed = 1f;

    public EnemyStats(EnemyStats stats)
    {
        this.hp = stats.hp;
        this.damage = stats.damage;
        this.exp_reward = stats.exp_reward;
        this.speed = stats.speed;
    }

    internal void ApplyProgress(float progress)
    {
        this.hp = (int)(hp * progress);
        this.damage = (int)(damage * progress);
    }
}

public class Enemy_scipt : MonoBehaviour, IDamagetable
{
    Transform targetPlayer;
    GameObject targetGameobject;
    Character_Scipt targetcharacter;

    Rigidbody2D rgdb2d;

    public EnemyStats stats;
    [SerializeField] EnemyData enemyData;

    private void Awake()
    {
        rgdb2d = GetComponent<Rigidbody2D>();
       
    }

    private void Start()
    {
        if(enemyData != null)
        {
            InitSprite(enemyData.animatedPrefab);
            SetStats(enemyData.stats);
            SetTarger(GameManager.instance.playerTransform.gameObject);
        }
    }

    public void SetTarger(GameObject target)
    {
        targetGameobject = target;
        targetPlayer = target.transform;
    }

    internal void UpdateStatsForProgress(float progress)
    {
        stats.ApplyProgress(progress);
    }

    private void FixedUpdate()
    {
        Vector3 direction = (targetPlayer.position - transform.position).normalized;
        rgdb2d.velocity = direction * stats.speed;
        Debug.Log(stats.hp);
  
    }

    internal void SetStats(EnemyStats stats)
    {
        this.stats = new EnemyStats(stats);
    }
  
    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject == targetGameobject)
        {
            Attack();
           
        }
    }

    private void Attack()
    {
        if(targetcharacter == null)
        {
            targetcharacter = targetGameobject.GetComponent<Character_Scipt>();
        }

        targetcharacter.TakeDamage(stats.damage);
        
    }

    public void Takedamage(int Damage)
    {
        stats.hp -= Damage;
        if (stats.hp <= 1)
        {
           
            Destroy(this.gameObject);
            Debug.Log("die");
            targetGameobject.GetComponent<Level_Scipt>().Addexp(stats.exp_reward);
        }

    }

    internal void InitSprite(GameObject animatedPrefab)
    {
        GameObject spriteObject = Instantiate(animatedPrefab);
        spriteObject.transform.parent = transform;
        spriteObject.transform.localPosition = Vector3.zero;  
    }
    
   
}

 