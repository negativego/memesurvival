using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterGameOver : MonoBehaviour
{
    public GameObject gameOverPanel;
    public GameObject MusicPanel;

    public void GameOver()
    {
        
        Debug.Log("Game Over");
        GetComponent<Playermove>().enabled = false;
        gameOverPanel.SetActive(true);
        MusicPanel.SetActive(false);
            }
}
