using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MassageSystem : MonoBehaviour
{
    public static MassageSystem instance;
   
    private void Awake()
    {
    instance = this;
    }


    [SerializeField] GameObject DamageMassage;

    List<TMPro.TextMeshPro> MessagePool;

    int objectCount = 10;
    int count;

    private void Start()
    {
        MessagePool = new List<TMPro.TextMeshPro>();
        for (int i = 0;i<objectCount;i++)
        {
            populate();
        }
    }

    public void populate()
    {
        GameObject go = Instantiate(DamageMassage, transform);
        MessagePool.Add(go.GetComponent<TMPro.TextMeshPro>());
        go.SetActive(false);

    }

    public void PostMessage(string text, Vector3 worldPosition)
    {
        MessagePool[count].gameObject.SetActive(true);
        MessagePool[count].transform.position = worldPosition;
        MessagePool[count].text = text;
        count += 1;

        if(count >= objectCount)
        {
            count = 0;
        }
    }
}
