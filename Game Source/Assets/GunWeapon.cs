using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunWeapon : WeaponDataBase
{
    [SerializeField] GameObject bullet;

    public override void Attack()
    {
        UpdateVectorOfAttack();
        GameObject thrownWeapon = Instantiate(bullet);
        thrownWeapon.transform.position = transform.position;
        ThrownProjectile thrownProjectile = thrownWeapon.GetComponent<ThrownProjectile>();
        thrownProjectile.setDirection(vectorOfAttack.x, vectorOfAttack.y);
        thrownProjectile.Damage = weaponStats.damage;
    }
}
