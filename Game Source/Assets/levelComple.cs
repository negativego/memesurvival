using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelComple : MonoBehaviour
{
    [SerializeField] float timeToCompletelevel;
    
    StageTime stageTime;
  
    PauseManager pauseManager;

    [SerializeField] GameWinpanel  levelCompletePanel;

    private void Awake()
    {
        stageTime = GetComponent<StageTime>();
        pauseManager = FindObjectOfType<PauseManager>();
        levelCompletePanel = FindObjectOfType<GameWinpanel>(true);
    }

    public void Update()
    {
        
        if(stageTime.time > timeToCompletelevel)
        {
            pauseManager.PauseGame();
            levelCompletePanel.gameObject.SetActive(true);
        }
    }
}
