using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
public class WeaponStats
{
    public int damage;
    public float timeToAttack;

    public WeaponStats(int damage,float timeToAttack)
    {
        this.damage = damage;
        this.timeToAttack = timeToAttack;
    }

    internal void Sum(WeaponStats weaponupgradeStats)
    {
        this.damage += weaponupgradeStats.damage;
        this.timeToAttack += weaponupgradeStats.timeToAttack;
    }
}

[CreateAssetMenu]
public class WeaponData : ScriptableObject
{
    public string Name;
    public WeaponStats stats;
    public GameObject WeaponBasePrefab;

    public List<UpgradeDATA> upgrades;
 

}
