using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageEventManage : MonoBehaviour
{
    [SerializeField] StageData StageData;
    Enemies_manager enemies_Manager;
    StageTime stageTime;
    int eventIndexer;
    PlayerWinManager playerWin;

    private void Awake()
    {
        stageTime = GetComponent<StageTime>();
    }

    private void Start()
    {
        playerWin = FindObjectOfType<PlayerWinManager>();
        enemies_Manager = FindObjectOfType<Enemies_manager>();
    }

    private void Update()
    {
        if(eventIndexer >= StageData.stageEvents.Count){return;}

        if(stageTime.time > StageData.stageEvents[eventIndexer].time)
        {
            switch(StageData.stageEvents[eventIndexer].eventType)
            {
                case StageEventType.SpawnEnemy:
                    
                        SpawnEnemy(false);
             
                    break;

                case StageEventType.SpawnObject:
                   
                        SpawnObject();
                  
                    break;

                case StageEventType.WinStage:
                    WinStage();
                    break;

                case StageEventType.SpawnEnemyBoss:
                    SpawnEnemyBoss();
                    break;
            }

            Debug.Log(StageData.stageEvents[eventIndexer].message);
            eventIndexer += 1;
        }
    }

    private void SpawnEnemyBoss()
    {
        SpawnEnemy(true);
    }

    private void WinStage()
    {
        playerWin.Win();
    }

    private void SpawnEnemy(bool bossEnemy)
    {
        StageEvent currentEvent = StageData.stageEvents[eventIndexer];
        enemies_Manager.AddGroupToSpawn(currentEvent.enemyTospawn, currentEvent.count, bossEnemy);
        
        if(currentEvent.isRepeatedEvent == true)
        {
            enemies_Manager.AddRepeatedSpawn(currentEvent, bossEnemy);
        }
    }

    private void SpawnObject()
    {
        Vector3 positionToSpawn = GameManager.instance.playerTransform.position;
        positionToSpawn += UtilityTools.GenerateRandomposition(new Vector2(5f, 5f));

        SpawnManager.instance.SpawnObject(positionToSpawn,StageData.stageEvents[eventIndexer].objectTospawn);  
    }
}
