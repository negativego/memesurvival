using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal_Pickup : MonoBehaviour, PickUpObject
{
    [SerializeField] int healAmount;
    public void PickUp(Character_Scipt character)
    {
        character.Heal(healAmount);
    }
}
