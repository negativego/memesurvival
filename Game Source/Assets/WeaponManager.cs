using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManager : MonoBehaviour
{
    [SerializeField] Transform WeaponObjectContainer;

    [SerializeField] WeaponData startingWeapon;

    List<WeaponDataBase> weapons;

    private void Awake()
    {
        weapons = new List<WeaponDataBase>();
    }

    private void Start()
    {
        Addweapon(startingWeapon);
    }

    public void Addweapon(WeaponData weaponData)
    {
        GameObject WeaponObject = Instantiate(weaponData.WeaponBasePrefab,WeaponObjectContainer);

        WeaponDataBase weaponDataBase = WeaponObject.GetComponent<WeaponDataBase>();

        weaponDataBase.SetData(weaponData);
        weapons.Add(weaponDataBase);

        Level_Scipt level = GetComponent<Level_Scipt>();
        if(level != null)
        {
            level.AddUpgradeIntotheListOfAvailableUpgrades(weaponData.upgrades);
        }
    }

    internal void UpgradeWeapon(UpgradeDATA upgradeData)
    {
        WeaponDataBase weaponToUpgrade = weapons.Find(wd => wd.Weapondata == upgradeData.weaponData);
        weaponToUpgrade.Upgrade(upgradeData);
    }
}
