using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeButton : MonoBehaviour
{
    [SerializeField] Image icon;
    [SerializeField] Image Disclip;

    public void Set(UpgradeDATA upgradeDATA)
    {
        icon.sprite = upgradeDATA.icon;
        Disclip.sprite = upgradeDATA.Discliption;
    }

    internal void Clean()
    {
        icon.sprite = null;
        Disclip.sprite = null;
    }
}
