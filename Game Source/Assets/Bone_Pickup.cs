using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bone_Pickup : MonoBehaviour, PickUpObject
{
    [SerializeField] int ExpAmount;
    public void PickUp(Character_Scipt character)
    {
        character.level.Addexp(ExpAmount);
    }
}