using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class ItemStats
{
    public int armor;

    internal void Sum(ItemStats stats)
    {
        armor += stats.armor;
    }
}

[CreateAssetMenu]
public class Item : ScriptableObject
{
    public string Name;
    public ItemStats stats;
    public List<UpgradeDATA> upgrades;

    public void Init(string name)
    {
        this.Name = Name;
        stats = new ItemStats();
        upgrades = new List<UpgradeDATA>();
    }

    public void Equip(Character_Scipt character)
    {
        character.armor += stats.armor;
    }

    public void UnEquip(Character_Scipt character)
    {
        character.armor -= stats.armor;
    }
}
