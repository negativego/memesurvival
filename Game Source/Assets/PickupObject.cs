using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface PickUpObject 
{
    public void PickUp(Character_Scipt character);
}
