using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UtilityTools : MonoBehaviour
{
    public static Vector3 GenerateRandomposition(Vector2 spawnArea)
    {

        Vector3 postion = new Vector3();

        float f = UnityEngine.Random.value > 0.5f ? -1f : 1f;
        if (UnityEngine.Random.value > 0.5f)
        {
            postion.x = UnityEngine.Random.Range(-spawnArea.x, spawnArea.x);
            postion.y = spawnArea.y * f;
        }

        else
        {
            postion.y = UnityEngine.Random.Range(-spawnArea.y, spawnArea.y);
            postion.y = spawnArea.x * f;
        }

        postion.z = 0;
        return postion;

    }
}

