using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradePanelManager : MonoBehaviour
{
    [SerializeField] GameObject panel;
    PauseManager pauseManager;

    [SerializeField] List<UpgradeButton> upgradeButtons;

    private void Awake()
    {
        pauseManager = GetComponent<PauseManager>();
    }

    private void Start()
    {
        HideButtons();
    }
    public void OpenPanel(List<UpgradeDATA> upgradeDATAs)
    {
        Clean();
        pauseManager.PauseGame();
        panel.SetActive(true);

        

        for (int i = 0; i < upgradeDATAs.Count; i++)
        {
            upgradeButtons[i].gameObject.SetActive(true);
            upgradeButtons[i].Set(upgradeDATAs[i]);
        }
    }

    public void Clean()
    {
        for(int i=0;i<upgradeButtons.Count;i++ )
        {
            upgradeButtons[i].Clean();
        }
    }

    public void Upgrade(int preesedButtonID)
    {
        Debug.Log("Press");
        GameManager.instance.playerTransform.GetComponent<Level_Scipt>().Upgrade(preesedButtonID);
        ClosePanel();
        
    }

    public void ClosePanel()
    {
        HideButtons();

        pauseManager.UnPauseGame();
        panel.SetActive(false);
    }

    private void HideButtons()
    {
        for (int i = 0; i < upgradeButtons.Count; i++)
        {
            upgradeButtons[i].gameObject.SetActive(false);
        }
    }

}
