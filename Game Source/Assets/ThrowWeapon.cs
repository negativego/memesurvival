using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowWeapon : WeaponDataBase
{
    [SerializeField] GameObject ThrownHamter;

    public override void Attack()
    {
        UpdateVectorOfAttack();
        GameObject thrownWeapon = Instantiate(ThrownHamter);
        thrownWeapon.transform.position = transform.position;
        ThrownProjectile thrownProjectile = thrownWeapon.GetComponent<ThrownProjectile>();
        thrownProjectile.setDirection(vectorOfAttack.x, vectorOfAttack.y);
        thrownProjectile.Damage = weaponStats.damage;
    }
}
