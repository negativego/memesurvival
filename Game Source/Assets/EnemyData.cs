using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

 
[CreateAssetMenu]

public class EnemyData : ScriptableObject
{
    public string Name;
    public GameObject animatedPrefab;
    public EnemyStats stats;
}
