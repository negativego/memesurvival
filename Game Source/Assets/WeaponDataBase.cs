using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DirectionOfAttack
{
    None,
    Forward,
    LeftRight,
    UpDown
}

public abstract class WeaponDataBase : MonoBehaviour
{
    Playermove playerMove;

    public WeaponData Weapondata;

    public WeaponStats weaponStats;
    
    float timer;

    public Vector2 vectorOfAttack;
    [SerializeField] DirectionOfAttack attackDirection;

    private void Awake()
    {
        playerMove = GetComponentInParent<Playermove>();
    }

    public void Update()
    {
        timer -= Time.deltaTime;

        if(timer < 0f)
        {
            Attack();
            timer = weaponStats.timeToAttack;
        }
    }

    public void ApplyDamage(Collider2D[] colliders)
    {
        for(int i =0; i < colliders.Length; i++ )
        {
            Debug.Log(colliders[i].gameObject.name);
            IDamagetable e = colliders[i].GetComponent<IDamagetable>();
            if(e != null)
            {

                PostDamage(weaponStats.damage, colliders[i].transform.position);
                e.Takedamage(weaponStats.damage);

            }

        }
    }

    public virtual void SetData(WeaponData wd)
    {
        Weapondata = wd;



        weaponStats = new WeaponStats(wd.stats.damage, wd.stats.timeToAttack);
    }
    public abstract void Attack();
    
    public virtual void PostDamage(int damage,Vector3 targetPosition)
    {
        MassageSystem.instance.PostMessage(damage.ToString(), targetPosition);
    }

    public void Upgrade(UpgradeDATA upgradeData)
    {
        weaponStats.Sum(upgradeData.weaponupgradeStats);
    }

    public void UpdateVectorOfAttack()
    {
        if(attackDirection == DirectionOfAttack.None)
        {
            vectorOfAttack = Vector2.zero;
            return;
        }

        switch (attackDirection)
        {
            case DirectionOfAttack.Forward:
                vectorOfAttack.x = playerMove.lastHorizontalCoupledVector;
                vectorOfAttack.y = playerMove.lastVerticalCoupledVector;
                break;
            case DirectionOfAttack.LeftRight:
                vectorOfAttack.x = playerMove.lastHorizontalDeCoupledVector;
                vectorOfAttack.y = 0f;
                break;
            case DirectionOfAttack.UpDown:
                vectorOfAttack.x = 0f;
                vectorOfAttack.y = playerMove.lastVerticalDeCoupledVector;
                break;
        }
        vectorOfAttack = vectorOfAttack.normalized;
    }
}
